package com.example.UserApplication.services;

import com.example.UserApplication.dto.TaskRequest;
import com.example.UserApplication.dto.TaskResponse;
import com.example.UserApplication.model.Organization;
import com.example.UserApplication.model.Task;
import com.example.UserApplication.repository.OrganizationRepository;
import com.example.UserApplication.repository.TaskRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final OrganizationRepository organizationRepository;
    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    public TaskResponse create(Long organizationId, TaskRequest taskRequest) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException());
        Task task = modelMapper.map(taskRequest, Task.class);
        organization.getTasks().add(task);
        task.setOrganization(organization);
        taskRepository.save(task);
        TaskResponse taskResponse = modelMapper.map(task, TaskResponse.class);
        return taskResponse;
    }
//    @Transactional
    public TaskResponse get(Long organizationId, Long taskId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException());
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new RuntimeException());
        return modelMapper.map(task, TaskResponse.class);
    }

    public Task update(Long organizationId, Long taskId, TaskRequest taskRequest){
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException());
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new RuntimeException());
        task.setOrganization(organization);
        task.setName(taskRequest.getName());
        task.setDescription(taskRequest.getDescription());
        organizationRepository.save(organization);
        return taskRepository.save(task);
    }
    public void delete(Long organizationId, Long taskId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException());
        taskRepository.deleteById(taskId);
    }
}