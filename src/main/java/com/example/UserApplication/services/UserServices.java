package com.example.UserApplication.services;

import com.example.UserApplication.dto.UserRequest;
import com.example.UserApplication.dto.UserResponse;
import com.example.UserApplication.model.Task;
import com.example.UserApplication.model.User;
import com.example.UserApplication.repository.TaskRepository;
import com.example.UserApplication.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServices {
    private final ModelMapper modelMapper;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
int [] numbers = {1,2,3,4,5,6,7,8,9,0};
    public UserResponse create(Long taskId, UserRequest userRequest) {
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new RuntimeException());
            User user = modelMapper.map(userRequest, User.class);
             task.getUsers().add(user);
             user.setTasks(task);
             userRepository.save(user);
            UserResponse userResponse = modelMapper.map(user, UserResponse.class);
            return userResponse;
    }
//    @Transactional
    public UserResponse get(Long taskId, Long userId) {
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new RuntimeException());
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException());
        return modelMapper.map(user, UserResponse.class);
    }
    public User update(Long taskId, Long userId, UserRequest userRequest) {
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new RuntimeException());
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException());
        user.setTasks(task);
        user.setName(user.getName());
        user.setSurname(user.getSurname());
        user.setEmail(user.getEmail());
        user.setPassword(user.getPassword());
        taskRepository.save(task);
        return userRepository.save(user);
    }
    public void delete(Long taskId,Long userId) {
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new RuntimeException());
        userRepository.deleteById(userId);
    }
}
