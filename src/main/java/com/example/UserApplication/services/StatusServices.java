package com.example.UserApplication.services;

import com.example.UserApplication.dto.StatusRequest;
import com.example.UserApplication.dto.StatusResponse;
import com.example.UserApplication.model.Status;
import com.example.UserApplication.repository.StatusRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatusServices {
    private final ModelMapper modelMapper;
    private final StatusRepository statusRepository;
    public StatusResponse create(StatusRequest statusRequest) {
        Status status = modelMapper.map(statusRequest, Status.class);
        status = statusRepository.save(status);
        return modelMapper.map(status, StatusResponse.class);

    }
    public void update(Long statusId, StatusRequest statusRequest) {
        Status status = statusRepository.findById(statusId).orElseThrow(() -> new RuntimeException());
        status.setName(status.getName());
        statusRepository.save(status);
    }
    @Transactional
    public StatusResponse get(Long statusId) {
        Status status = statusRepository.findById(statusId).orElseThrow(() -> new RuntimeException());
        return modelMapper.map(status, StatusResponse.class);
    }
    public void delete(Long statusId) {
        statusRepository.deleteById(statusId);
    }
}
