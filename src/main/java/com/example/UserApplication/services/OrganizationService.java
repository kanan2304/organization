package com.example.UserApplication.services;

import com.example.UserApplication.dto.OrganizationRequest;
import com.example.UserApplication.dto.OrganizationResponse;
import com.example.UserApplication.model.Organization;
import com.example.UserApplication.repository.OrganizationRepository;
import com.example.UserApplication.repository.TaskRepository;
import com.example.UserApplication.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public OrganizationResponse create(OrganizationRequest organizationRequest) {
        Organization organization = modelMapper.map(organizationRequest, Organization.class);
        organizationRepository.save(organization);
        return modelMapper.map(organization, OrganizationResponse.class);
    }
//    @Transactional
    public OrganizationResponse get(Long organizationId){
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException());
        return modelMapper.map(organization, OrganizationResponse.class);
    }
    public void update(Long organizationId, OrganizationRequest organizationRequest){
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException());
        organization.setName(organizationRequest.getName());
        organizationRepository.save(organization);
    }

    public void delete(Long organizationId) {
        organizationRepository.deleteById(organizationId);
    }

//    public Optional<Organization> getAllOrganizations(Long organizationId) {
//       return organizationRepository.findById(organizationId);
//    }
}
