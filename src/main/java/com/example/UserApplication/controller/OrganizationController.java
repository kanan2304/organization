package com.example.UserApplication.controller;

import com.example.UserApplication.dto.OrganizationRequest;
import com.example.UserApplication.dto.OrganizationResponse;
import com.example.UserApplication.model.Organization;
import com.example.UserApplication.repository.OrganizationRepository;
import com.example.UserApplication.services.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/organization")
public class OrganizationController {
    private final OrganizationService organizationService;
    private final OrganizationRepository organizationRepository;
    @PostMapping
    public OrganizationResponse create(@RequestBody OrganizationRequest organizationRequest){
        return organizationService.create(organizationRequest);
    }
    @GetMapping("/{organizationId}")
    public OrganizationResponse get(@PathVariable Long organizationId){
        return organizationService.get(organizationId);
    }

    @GetMapping
    public List<Organization> findAllOrganizations(){
        return organizationRepository.findAll();
//        return organizationService.getAllOrganizations(organizationId);
    }

    @PutMapping("/{organizationId}")
    public void update(@PathVariable Long organizationId, @RequestBody OrganizationRequest organizationRequest ){
        organizationService.update(organizationId, organizationRequest);
    }
    @DeleteMapping("/{organizationId}")
    public void delete(@PathVariable Long organizationId){
        organizationService.delete(organizationId);
    }
}
