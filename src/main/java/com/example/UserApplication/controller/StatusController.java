package com.example.UserApplication.controller;

import com.example.UserApplication.dto.StatusRequest;
import com.example.UserApplication.dto.StatusResponse;
import com.example.UserApplication.services.StatusServices;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class StatusController {
    private final StatusServices statusServices;
    @PostMapping("/{taskId}/status")
    public StatusResponse create(@PathVariable Long taskId, @RequestBody StatusRequest statusRequest){
        return statusServices.create(statusRequest);
    }
    @PutMapping("/{taskId}/status/{statusId}")
    public void update(@PathVariable Long taskId, @PathVariable Long statusId, @RequestBody StatusRequest statusRequest){
        statusServices.update(statusId, statusRequest);
    }
    @GetMapping("/{statusId}")
    public StatusResponse get(@PathVariable Long statusId){
        return statusServices.get(statusId);
    }
    @DeleteMapping("/{statusId}")
    public void delete(@PathVariable Long statusId){
        statusServices.delete(statusId);
    }

}
