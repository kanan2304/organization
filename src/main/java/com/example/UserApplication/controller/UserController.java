package com.example.UserApplication.controller;

import com.example.UserApplication.dto.UserRequest;
import com.example.UserApplication.dto.UserResponse;
import com.example.UserApplication.model.User;
import com.example.UserApplication.repository.UserRepository;
import com.example.UserApplication.services.UserServices;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.LifecycleState;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class UserController {
    private final UserServices userService;
    private final UserRepository userRepository;

    @PostMapping("/{taskId}")
    public UserResponse create(@PathVariable Long taskId, @RequestBody UserRequest userRequest){
         return userService.create(taskId, userRequest);
    }
    @GetMapping("/{taskId}/user/{userId}")
    public UserResponse get(@PathVariable Long taskId, @PathVariable Long userId){
        return userService.get(taskId, userId);
    }
//    @GetMapping("{taskId}/user/")
//    public List<User> getAll(){
//        return userRepository.findAllUsers();
//    }
    @PutMapping("/{taskId}/user/{userId}")
    public User update(@PathVariable Long taskId, @PathVariable Long userId, @RequestBody UserRequest userRequest){
        return userService.update(taskId, userId, userRequest);
    }
    @DeleteMapping("/{taskId}/user/{userId}")
    public void delete(@PathVariable Long taskId,@PathVariable Long userId){
        userService.delete(taskId, userId);
    }
}