package com.example.UserApplication.controller;

import com.example.UserApplication.dto.TaskRequest;
import com.example.UserApplication.dto.TaskResponse;
import com.example.UserApplication.model.Task;
import com.example.UserApplication.repository.TaskRepository;
import com.example.UserApplication.services.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/organization")
public class TaskController {
    private final TaskRepository taskRepository;
    private final TaskService taskService;
    @PostMapping("/{organizationId}")
    public TaskResponse create(@PathVariable Long organizationId ,@RequestBody TaskRequest taskRequest){
        return taskService.create(organizationId,taskRequest);
    }
    @GetMapping("/{organizationId}/task/{taskId}")
    public TaskResponse get(@PathVariable Long organizationId, @PathVariable Long taskId){
      return taskService.get(organizationId, taskId);
    }
//    @GetMapping()
//    public List<Task> getAll(){
//        return taskRepository.findAllTasks();
//    }
    @PutMapping("/{organizationId}/task/{taskId}")
    public Task update(@PathVariable Long organizationId, @PathVariable Long taskId, @RequestBody TaskRequest taskRequest){
        return taskService.update(organizationId, taskId, taskRequest);
    }
    @DeleteMapping("/{organizationId}/task/{taskId}")
    public void delete(@PathVariable Long organizationId, @PathVariable Long taskId){
        taskService.delete(organizationId, taskId);
    }
}
