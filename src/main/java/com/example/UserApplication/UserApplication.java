package com.example.UserApplication;

import com.example.UserApplication.model.Organization;
import com.example.UserApplication.repository.OrganizationRepository;
import com.example.UserApplication.repository.TaskRepository;
import com.example.UserApplication.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
public class UserApplication implements CommandLineRunner {
	private final OrganizationRepository organizationRepository;
	private final UserRepository userRepository;
	private final TaskRepository taskRepository;

	public static void main(String[] args)  {
		SpringApplication.run(UserApplication.class, args);
	}
	@Override
	@Transactional
	public void run(String... args) throws Exception {

	}
}