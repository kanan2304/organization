package com.example.UserApplication.dto;

import com.example.UserApplication.model.Task;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResponse {
    Long id;
    String name;
    String deadline;
    String description;


}
