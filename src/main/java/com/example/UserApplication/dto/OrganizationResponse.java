package com.example.UserApplication.dto;

import com.example.UserApplication.model.Organization;
import com.example.UserApplication.model.Task;
import com.example.UserApplication.model.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponse {
    Long id;
    String name;
    @Builder.Default
    List<Task> tasks = new ArrayList<>();
    @Builder.Default
    List<User> users = new ArrayList<>();

}
