package com.example.UserApplication.repository;

import com.example.UserApplication.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface StatusRepository extends JpaRepository<Status, Long> {
}
