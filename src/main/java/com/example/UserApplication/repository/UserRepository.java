package com.example.UserApplication.repository;

import com.example.UserApplication.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long > {
//    List<User> /findAllUsers();
}
