FROM openjdk:18-alpine
COPY build/libs/UserApplication-0.0.1-SNAPSHOT.jar /userapp/
CMD ["java", "-jar", "/userapp/UserApplication-0.0.1-SNAPSHOT.jar"]